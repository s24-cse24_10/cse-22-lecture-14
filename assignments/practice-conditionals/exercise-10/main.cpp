#include <iostream>

using namespace std;

int main(){
    /* your code goes here */

    string rank;
    string suit;

    cin >> rank >> suit;

    string rankName;
    string suitName;

    if (rank == "A") {
        rankName = "Ace";
    } else if (rank == "2") {
        rankName = "Two";
    } else if (rank == "3") {
        rankName = "Three";
    } else if (rank == "4") {
        rankName = "Four";
    } else if (rank == "5") {
        rankName = "Five";
    } else if (rank == "6") {
        rankName = "Six";
    } else if (rank == "7") {
        rankName = "Seven";
    } else if (rank == "8") {
        rankName = "Eight";
    } else if (rank == "9") {
        rankName = "Nine";
    } else if (rank == "10") {
        rankName = "Ten";
    } else if (rank == "J") {
        rankName = "Jack";
    } else if (rank == "Q") {
        rankName = "Queen";
    } else if (rank == "K") {
        rankName = "King";
    }

    if (suit == "C") {
        suitName = "Clubs";
    } else if (suit == "D") {
        suitName = "Diamonds";
    } else if (suit == "H") {
        suitName = "Hearts";
    } else if (suit == "S") {
        suitName = "Spades";
    }

    if (rankName == "" || suitName == "") {
        cout << "Unknown card." << endl;
    } else {
        cout << rankName << " of " << suitName << endl;
    }
    

    return 0;
}
