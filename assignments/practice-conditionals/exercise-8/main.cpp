#include <iostream>

using namespace std;

int main(){
    /* your code goes here */

    int x;
    int y;
    int z;

    cin >> x >> y >> z;

    if (x > y && x > z) {
        cout << "Largest number is: " << x << endl;
    } else if (y > x && y > z) {
        cout << "Largest number is: " << y << endl;
    } else if (z > x && z > y) {
        cout << "Largest number is: " << z << endl;
    }

    return 0;
}
