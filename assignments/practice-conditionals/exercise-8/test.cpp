#include <igloo/igloo.h>
#include <cstdlib>
#include <iostream>

using namespace igloo;

std::string exec(std::string command) {
   char buffer[128];
   std::string result = "";

   // Open pipe to file
   FILE* pipe = popen((command + " 2>&1").c_str(), "r");
   if (!pipe) {
      return "popen failed!";
   }

   // read till end of process:
   while (!feof(pipe)) {

      // use buffer to read and add to result
      if (fgets(buffer, 128, pipe) != NULL)
         result += buffer;
   }

   pclose(pipe);
   result.erase(result.find_last_not_of(" \t\n\r\f\v") + 1);
   return result;
}

std::string compile_run(std::string filename){
    std::string compile = exec("g++ -o temp " + filename);
    std::string output = compile;
    if (compile == ""){
        output = exec("./temp");
        system("rm -f temp");
    }

    return output;
}

std::string compile_run(std::string filename, std::string input){
    std::string compile = exec("g++ -o temp " + filename);
    std::string output = compile;
    if (compile == ""){
        output = exec("echo \"" + input + "\" | ./temp");
        system("rm -f temp");
    }

    return output;
}

std::string run_executable(std::string input) {
    std::string output = exec("echo \"" + input + "\" | ./temp");
    return output;
}

Context(LargestNumber){
    static void SetUpContext(){
        exec("g++ main.cpp -o temp");
    }
    static void TearDownContext() {
        system("rm -rf temp");
    }

    Spec(test_1_2_3){
        std::string input = "1\n2\n3";
        std::string result = run_executable(input);
        Assert::That(result, Equals("Largest number is: 3"));
    }
    Spec(test_11_13_12){
        std::string input = "11\n13\n12";
        std::string result = run_executable(input);
        Assert::That(result, Equals("Largest number is: 13"));
    }
    Spec(test_31_21_11){
        std::string input = "31\n21\n11";
        std::string result = run_executable(input);
        Assert::That(result, Equals("Largest number is: 31"));
    }
};

int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}










