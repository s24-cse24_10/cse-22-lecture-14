#include <iostream>

using namespace std;

int main(){
    /* your code goes here */

    string letter;
    cin >> letter;

    // A E I O U
    if (letter == "A" || letter == "E" || letter == "I" || letter == "O" || letter == "U" || letter == "a" || letter == "e" || letter == "i" || letter == "o" || letter == "u") {
        cout << "Letter is a vowel." << endl;
    } else {
        cout << "Letter is not a vowel." << endl;
    }

    return 0;
}
