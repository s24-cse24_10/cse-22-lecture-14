#include <iostream>

using namespace std;

int main(){
    /* your code goes here */

    int x;
    int y;
    string op;

    cin >> x >> y >> op;

    if (op == "+") {
        cout << x << " " << op << " " << y << " = " << (x + y) << endl;
    } else if (op == "-") {
        cout << x << " " << op << " " << y << " = " << (x - y) << endl;
    } else if (op == "*") {
        cout << x << " " << op << " " << y << " = " << (x * y) << endl;
    } else if (op == "/") {
        cout << x << " " << op << " " << y << " = " << (x / y) << endl;
    } else if (op == "%") {
        cout << x << " " << op << " " << y << " = " << (x % y) << endl;
    } else {
        cout << "Invalid operation." << endl;
    }



    return 0;
}
