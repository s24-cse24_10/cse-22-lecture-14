#include <iostream>
#include <iomanip>

using namespace std;

int main(){
    /* your code goes here */
    float bill;
    int people;
    float tip;

    cin >> bill >> people >> tip;

    float split = (bill + (bill * tip)) / 4;
    // float split = total / people;

    cout << fixed << setprecision(2);
    cout << "Each person owes: $" << split << endl;

    return 0;
}
